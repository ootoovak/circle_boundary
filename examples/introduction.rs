extern crate circle_boundary;

use circle_boundary::calculate;

fn main() {
    let returned_boundary = calculate(0, 0, 8);

    println!("The circle boundary returned is:\n{:#?}", returned_boundary);
}
