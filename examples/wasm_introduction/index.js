const load_wasm =
    import ('circle_boundary/circle_boundary.js')

load_wasm.then(wasm => {
    let result = wasm.web_calculate(0, 0, 8)
    console.log('The calculated circle boundary:', result)
})