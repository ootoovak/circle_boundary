# Circle Boundary

A single purpose library. This library's aim is to return a circle boundary as represented in discrete pixels as shown in the following diagram.

![five radius circle boundary](https://gitlab.com/ootoovak/circle_boundary/raw/master/images/5-radius.png)

## Usage

Include the library into your application with the following line at the top of your main file.

```rust
extern crate circle_boundary;
```

And then get access to the public `calculate` function by adding the following line at the top of the file you want to use it in.

```rust
use circle_boundary::calculate;
use circle_boundary::Boundary;
```

Then you can use the calculate function to get a vector of boundary data structures back. Each boundary structure holds a pair of cartesian coordinates (x and y) where x is represented by a single number indicating it's position, and y is represented by a range to show where on the y-axis the boundary applies.

The `calculate` function expects the origin as represented by the first two arguments, a x value as an `i32` integer and a y value as an `i32` integer. The final argument is the radius of the circle you want a boundary for as represented as a `i32` integer.

```rust
let bounds = calculate(0, 0, 3);
```

As noted above the value returned is a vector that represents the boundary as such:

```rust
vec![
    Boundary { x: -3,  y: -1..1 },
    Boundary { x: -2,  y: -2..2 },
    Boundary { x: -1,  y: -3..3 },
    Boundary { x:  0,  y: -3..3 },
    Boundary { x:  1,  y: -3..3 },
    Boundary { x:  2,  y: -2..2 },
    Boundary { x:  3,  y: -1..1 },
];
```

Which could be represented as in the following diagram. Where the dark grey square represents the origin, the green squares represent the lower bounds of the y range, and the red squares represent the upper bounds of the y range.

![three radius circle boundary](https://gitlab.com/ootoovak/circle_boundary/raw/master/images/3-radius.png)

### Off origin example

As noted the `calculate` function can be given coordinates that offset the results from a `(0,0)` origin. If you pass in an offset like:

```rust
calculate(9, 3, 7);
```

This returns the following vector:

```rust
vec![
    Boundary { x: 2, y: 1..5 },
    Boundary { x: 3, y: -1..7 },
    Boundary { x: 4, y: -2..8 },
    Boundary { x: 5, y: -3..9 },
    Boundary { x: 6, y: -3..9 },
    Boundary { x: 7, y: -4..10 },
    Boundary { x: 8, y: -4..10 },
    Boundary { x: 9, y: -4..10 },
    Boundary { x: 10, y: -4..10 },
    Boundary { x: 11, y: -4..10 },
    Boundary { x: 12, y: -3..9 },
    Boundary { x: 13, y: -3..9 },
    Boundary { x: 14, y: -2..8 },
    Boundary { x: 15, y: -1..7 },
    Boundary { x: 16, y: 1..5 },
];
```

Which represents the following circle boundary where the black square represents the `(0,0)` origin:

![seven radius circle boundary with offset](https://gitlab.com/ootoovak/circle_boundary/raw/master/images/7-radius-offset.png)

## Examples

For more examples on how to use this library you can look in the `examples/` folder in this repository. You can run each one with the following command:

```bash
cargo run --example <example-name>
```

For example:

```bash
cargo run --example introduction
```

Would run the introductory example file `examples/introduction.rs`.

## Development

### Building for Rust

Have Rust and Cargo installed, then run:
```
cargo build
```

### Building for the Web

Install [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/), then run:
```
wasm-pack build
```

- Copy over the `README_NPM.md` file into the `/pkg` folder as `README.md`.
- Make sure the `images` file is copied over into the generated `/pkg` folder as well.

### Runing the tests

Use the Cargo command:

```
cargo test
```
