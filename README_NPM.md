# Circle Boundary

A single purpose library. This library's aim is to return a circle boundary as represented in discrete pixels as shown in the following diagram.

![five radius circle boundary](https://gitlab.com/ootoovak/circle_boundary/raw/master/images/5-radius.png)

## Usage

Include the library into your application with the following:

```javascript
const load_wasm = import ('circle_boundary/circle_boundary.js')
```

Then you can use the calculate function to get a javascript object of boundary data structures back. Each boundary structure holds a pair of cartesian coordinates (x and y) where x is represented by a single number indicating it's position, and y is represented by a range to show where on the y-axis the boundary applies.

The `web_calculate` function expects the origin as represented by the first two arguments, a x value as an integer and a y value as an integer. The final argument is the radius of the circle you want a boundary for as represented as an integer as well.

Because of how Webpack loads the WASM you must wait till it is asynchronous loaded before you can call the function like so:

```javascript
load_wasm.then(wasm => {
    let result = wasm.web_calculate(0, 0, 3)
})
```

As noted above the value returned is a javascript object that represents the boundary as such:

```javascript
[
  {
    "x": -3,
    "y": {
      "start": -1,
      "end": 1
    }
  },
  {
    "x": -2,
    "y": {
      "start": -2,
      "end": 2
    }
  },
  {
    "x": -1,
    "y": {
      "start": -3,
      "end": 3
    }
  },
  {
    "x": 0,
    "y": {
      "start": -3,
      "end": 3
    }
  },
  {
    "x": 1,
    "y": {
      "start": -3,
      "end": 3
    }
  },
  {
    "x": 2,
    "y": {
      "start": -2,
      "end": 2
    }
  },
  {
    "x": 3,
    "y": {
      "start": -1,
      "end": 1
    }
  }
]
```

Which could be represented as in the following diagram. Where the dark grey square represents the origin, the green squares represent the lower bounds of the y range, and the red squares represent the upper bounds of the y range.

![three radius circle boundary](https://gitlab.com/ootoovak/circle_boundary/raw/master/images/3-radius.png)

### Off origin example

As noted the `web_calculate` function can be given coordinates that offset the results from a `(0,0)` origin. If you pass in an offset like:

```javascript
web_calculate(9, 3, 7);
```

This returns the following vector:

```javascript
[
  {
    "x": 2,
    "y": {
      "start": 1,
      "end": 5
    }
  },
  {
    "x": 3,
    "y": {
      "start": -1,
      "end": 7
    }
  },
  {
    "x": 4,
    "y": {
      "start": -2,
      "end": 8
    }
  },
  {
    "x": 5,
    "y": {
      "start": -3,
      "end": 9
    }
  },
  {
    "x": 6,
    "y": {
      "start": -3,
      "end": 9
    }
  },
  {
    "x": 7,
    "y": {
      "start": -4,
      "end": 10
    }
  },
  {
    "x": 8,
    "y": {
      "start": -4,
      "end": 10
    }
  },
  {
    "x": 9,
    "y": {
      "start": -4,
      "end": 10
    }
  },
  {
    "x": 10,
    "y": {
      "start": -4,
      "end": 10
    }
  },
  {
    "x": 11,
    "y": {
      "start": -4,
      "end": 10
    }
  },
  {
    "x": 12,
    "y": {
      "start": -3,
      "end": 9
    }
  },
  {
    "x": 13,
    "y": {
      "start": -3,
      "end": 9
    }
  },
  {
    "x": 14,
    "y": {
      "start": -2,
      "end": 8
    }
  },
  {
    "x": 15,
    "y": {
      "start": -1,
      "end": 7
    }
  },
  {
    "x": 16,
    "y": {
      "start": 1,
      "end": 5
    }
  }
]
```

Which represents the following circle boundary where the black square represents the `(0,0)` origin:

![seven radius circle boundary with offset](https://gitlab.com/ootoovak/circle_boundary/raw/master/images/7-radius-offset.png)

## Examples

For more example on how to use this library you can look in the `examples/` folder in this repository under `wasm_introduction`.

## Development

This package is developed in Rust. The library and development instructions can be found [here](https://gitlab.com/ootoovak/circle_boundary).
